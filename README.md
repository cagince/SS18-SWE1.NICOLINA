# Wofür wird GIT verwendet?

In dieser LV werden all Ihre Übungsausarbeitungen über GitLab abgegeben bzw. eingereicht. Wichtig hierbei ist, dass wir Ihren Masterbranch als maßgeblich für die Bewertung, etc. ansehen. Die jeweiligen Punkte und Prüfungsergebnisse sind weiterhin in Moodle ersichtlich.

# Wie erhalte ich lokalen Zugriff auf dieses Repository?

Um optimal mit diesem Repository zu arbeiten sollten Sie es auf Ihr lokales Arbeitsgerät spiegeln. Verwenden Sie hierzu den Befehl `git clone UrlIhresRepositories`. Die Url Ihres Repositories findet sich im Kopf dieser Webseite direkt unter dem Namen des Repositores und sollte vergleichbar sein zu `https://git01lab.cs.univie.ac.at/.....`. 

# Wie nütze ich dieses Repository?

Clonen Sie hierzu dieses Repository wie oben angegeben. Danach können Sie mit `git add`, `commit`, `push`, etc. damit arbeiten. Optimalerweise legen Sie hierzu nach dem initialen clone Ihren Namen und Ihre E-Mail-Adresse fest sodass alle Commits Ihnen direkt zugeordnet werden können. Verwenden Sie hierzu folgende Befehle:

> `git config --global user.name "Mein Name"`

> `git config --global user.email a123456@univie.ac.at`


Weitere Informationen über dem Umgang mit GIT sind in den hierzu passenden Moodlefolien erhältlich bzw. wurden während dem Git Hands-On besprochen. Zusätzlich können Sie Git auch interaktiv erlernen unter: https://try.github.io

# Welche Inhalte sind vorgegeben und wofür sind diese gedacht?

Es wurden mehrere **Ordner** sowie eine **.gitignore** Datei vorgegeben. Letztere dient dazu Ihr Repository nicht mit "unnötigen" Dateien zu befüllen welche es erschweren würde Ihr Projekt während der Abgabegespräche in die Entwicklungsumgebungen der Lektoren zu importieren (temporäre Dateien, etc.). Ändern Sie diese Datei daher nicht bzw. nur sehr behutsam. 

Die vorgebenen Ordner sind wie folgt zu verwenden:
* Dokumentation - Nützen Sie diesen Ordner um Ihre Dokumentation abzulegen bzw. abzugeben. Dies ist insbesondere für Teilaufgabe 1 relevant da Sie hierzu ihre Ausarbeitung (das PDF) hier hinterlegen können um diese abzugeben.
* Executables - Hinterlegen Sie hier die finalen kompilierten Abgaben Ihrer Implementierung von Teilaufgabe 2 und Teilaufgabe 3. Diese sollten .jar Dateien seien welche sich mit java -jar <NameDerJarDatei> exekutieren lassen. Prüfen Sie daher auch ob dies der Fall ist! 
* Source - Nützen Sie diesen Ordner um die Implementierung von Teilaufgabe 2 und Teilaufgabe 3 abzulegen (daher Sourcecode, Bibliotheken, etc.). Nützen Sie diesen Ordner daher als Eclipse Workspace. Falls Sie Bibliotheken manuell einbinden achten Sie bitte darauf relative und keine absoluten Pfade zu verwenden da ansonsten es sehr erschwert wird Ihren Sourcecode während der Abgabegespräche zu berücksichtigen. In diesem Ordner können Sie auch den initialen Entwurf der Architektur für Teilaufgabe 1 ablegen um danach direkt im Anschluss für Teilaufgabe 2 und 3 daran weiterzuarbeiten bzw. diese für das Abgabegespräch von Teilaufgabe 1 abzugeben.
 
Erstellen Sie bitte keine zusätzlichen Ordner im Wurzelverzeichnis dieses Repositories und verändern Sie nicht die Namen, etc. der vorgegebenen Ordner. Zusätzliche Ordner, etc. können Sie als Unterordner in den vorgegebenen Ordnern erstellen.

# Welche Funktionen sollen nicht genutzt werden?

GitLab ist eine mächtige Software die es erlaubt über zahlreiche Einstellungen angepasst zu werden. Wir würden dazu Raten diese Möglichkeiten nicht zu nützen da unbedachte Aktionen hierbei auch negative Auswirkungen haben können. Verwenden Sie daher optimalerweise einfach die vergebenen Einstellungen.