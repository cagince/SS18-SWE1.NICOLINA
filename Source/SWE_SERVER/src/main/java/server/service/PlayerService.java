package server.service;

import java.util.HashMap;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import server.model.Player;

@Service
public class PlayerService {

	private final Logger logger = Logger.getLogger(PlayerService.class.getName());
	HashMap<String, Player> players = new HashMap();
	
	
	public Player createPlayer(String firstname, String lastname, String matr) {
		Player player = new Player(firstname, lastname, matr);
		players.put(player.getId(), player);
		return player;
	}
	
	
	
}
