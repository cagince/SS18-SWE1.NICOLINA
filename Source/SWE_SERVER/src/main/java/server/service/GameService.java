package server.service;

import java.util.HashMap;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import messages.PlayerIdentifier;
import messages.ResponseEnvelope;
import messages.halfMapRegister.request.HalfMap.NewMapNodes;
import messages.playerRegister.response.Statevalues;
import server.model.Game;
import server.model.HalfMap;
import server.model.Player;

@Service
public class GameService {
	
	private final Logger logger = Logger.getLogger(GameService.class.getName());
	
	// saves the games
	// wie eine datenbank 
	HashMap<String, Game> games = new HashMap();
	
	/**
	 * Creates a new Game
	 * @return new Game
	 */
	public Game createGame() {
		Game game = new Game();
		games.put(game.getId(), game);
		logger.info("A new Game with id: " + game.getId() + " has been created");
		return game;
	}
	
	
	/**
	 * adds new player to game
	 * @param game_id id of the game
	 * @param player new player to add the game
	 * @throws Exception if something went wrong while adding
	 */
	public void addUserToGame(String game_id, Player player) throws Exception {
		Game game = games.get(game_id);
		
		// Check if game exists
		if (game == null) throw new Exception("Game does not exist");
		// check if there is a place for new user
		if (game.getPlayer1() != null && game.getPlayer2() != null) throw new Exception("Game is already full");
		
		// if game exists and not full add the player to game
		game.addPlayer(player);
	}
	
	
	/**
	 * adds the player to game  -> calls addUserToGame first
	 * -> returns ResponseEnvelope with PlayerIdentifier if succeeds
	 * -> returns ResponseEnvelope with Error if not
	 * @param game_id
	 * @param player
	 * @return
	 */
	public ResponseEnvelope registerPlayer(String game_id, Player player) {
		try {
			addUserToGame(game_id, player); // tries to add the user 
			logger.info("a New Player has been added to " + game_id);
			PlayerIdentifier pid = new PlayerIdentifier(player.getId());
			return new ResponseEnvelope<PlayerIdentifier>(pid);
		} catch (Exception e) {
			logger.warning("Something went wrong adding user to game: " + e.getMessage());
			// Game does not exist -> GameDoesNotExist
			return new ResponseEnvelope(toCamelCase(e.getMessage()), e.getMessage());
		}
	}
	
	
	// 
	public ResponseEnvelope submitMap(String game_id, String player_id, NewMapNodes nodes) {
		Game game = this.games.get(game_id); // get Game
		if (game == null) {
			// Game Does Not Exist.
			logger.warning("Game Does Not Exist");
			return new ResponseEnvelope("GameDoesNotExist", "Game Does not Exist");
		} else if (game.getPlayer1() != null && game.getPlayer2() != null) {
			// game exists and both players are registered
			
			logger.info("Submitting map to game: " + game_id);
			if (!(
					player_id.equals(game.getPlayer2())  // player ist player2
					&& game.getHalfMap1() == null) // halfmap 1 existiert nicht
					) {
				// entweder player ist player1 
				// oder player ist player 2 und player 1 hat halfmap schon hinzugefügt
				
				try {
					HalfMap new_halfmap = new HalfMap(nodes); // Halfmap wird erstellt und business rules werden geprüft
					if (player_id.equals(game.getPlayer1())) {
						// player is player1
						game.setHalfMap1(new_halfmap);
					} else {
						// player is player2
						game.setHalfMap2(new_halfmap);
					}
					return new ResponseEnvelope(); // schickt ein response mit status OK
				} catch (Exception e) {
					// there was an error while creating map
					logger.warning("Something went wrong adding halfmap: " + e.getMessage());
					// wieder mit camelCase
					return new ResponseEnvelope(toCamelCase(e.getMessage()), e.getMessage());
				}
			} else {
				// player 2 tries to submit halfmap but player 1 didnt submit yet.
				logger.warning("Player 1 has to submit it's map first.");
				return new ResponseEnvelope("NotYourTurn", "You should wait until other player submits halfmap");
			}
		} else {
			// game exists but not both players are registered.
			logger.warning("Should wait for all players to register");
			return new ResponseEnvelope("NotYourTurn", "You should wait until players are full");
		}
	}
	
	public void submitMove(String game_id) {
		logger.info("Submitting move to game: " + game_id);
	}
	
	// Game does not exist -> GameDoesNotExist
	public String toCamelCase(String s) {
		String[] parts = s.split(" ");
		String camelCaseString = "";
		for (String part : parts){
			camelCaseString = camelCaseString + toProperCase(part);
		}
		return camelCaseString;
	}
	static String toProperCase(String s) {
	    return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}
}

