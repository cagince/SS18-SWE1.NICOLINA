package server.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import messages.PlayerIdentifier;
import messages.ResponseEnvelope;
import messages.halfMapRegister.request.HalfMap.NewMapNodes;
import messages.halfMapRegister.request.HalfMap.NewMapNodes.NewMapNode;
import messages.halfMapRegister.request.Terraintype;

import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import server.model.Game;
import server.model.Player;




@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTests {
	
	
	@Autowired
    @MockBean
    private PlayerService playerService;

    @MockBean
    private GameService gameService;
    
    
	/**
	 * creates a new player
	 */
	@Test
	public void createPlayer() throws Exception { 
		given(this.playerService.createPlayer("john", "doe", "0123"))
		.willReturn(new Player("john", "does", "0123"));
	}
	
	/**
	 * creates a new game
	 */
	@Test
	public void createGame() throws Exception {
		given(this.gameService.createGame()).willReturn(new Game());
	}
	
	
	/**
	 *  Successfully registers a new player
	 */
	@Test
	public void registerPlayerSuccess() throws Exception {
		Game game = new Game();
		Player player = new Player("john", "doe", "0123");
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		given(this.gameService.registerPlayer(game.getId(), player))
		.willReturn(new ResponseEnvelope<PlayerIdentifier>(new PlayerIdentifier(player.getId())));
	}
	
	/**
	 *  Fails to register player 
	 *  because game doesn't exist
	 */
	@Test
	public void registerPlayerGameDoesntExist() throws Exception {
		Player player = new Player("john", "doe", "0123");
		HashMap<String, Game> games = new HashMap();
		this.gameService.games = games;
		given(this.gameService.registerPlayer("sometext", player))
		.willReturn(new ResponseEnvelope("GameDoesNotExist", "Game does not exist"));
	}
	
	/**
	 *  Fails to register player 
	 *  because game is already full
	 */
	@Test
	public void registerPlayerFail() throws Exception {
		Game game = new Game();
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		Player player1 = new Player("john", "doe", "0123");
		Player player2 = new Player("jane", "doe", "0123");
		Player player3 = new Player("samuel", "doe", "0123");
		this.gameService.registerPlayer(game.getId(), player1);
		this.gameService.registerPlayer(game.getId(), player2);
		given(this.gameService.registerPlayer(game.getId(), player3))
		.willReturn(new ResponseEnvelope("GameIsAlreadyFull", "Game is already full"));
	}
	
	/**
	 *  Fails to submit Halfmap
	 *  because there are wrong number of NewMapNodes <- not 32.
	 */
	@Test 
	public void submitHalfMapFails() throws Exception {
		Game game = new Game();
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		Player player1 = new Player("john", "doe", "0123");
		Player player2 = new Player("jane", "doe", "0123");
		this.gameService.registerPlayer(game.getId(), player1);
		this.gameService.registerPlayer(game.getId(), player2);
		given(this.gameService.submitMap(game.getId(), player1.getId(), new NewMapNodes()))
		.willReturn(new ResponseEnvelope("NumberOfTerrainsAreWrong", "Number of terrains are wrong"));
	}
	
		
	/**
	 *  Fails to submit Halfmap
	 *  because player 2 has not registered for the game yet
	 */
	@Test 
	public void shouldWaitEveryoneToRegister() throws Exception {
		Game game = new Game();
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		Player player1 = new Player("john", "doe", "0123");
		this.gameService.registerPlayer(game.getId(), player1);
		given(this.gameService.submitMap(game.getId(), player1.getId(), new NewMapNodes()))
		.willReturn(new ResponseEnvelope("NotYourTurn", "You should wait until players are full"));
	}
	
	
	/**
	 *  Fails to submit Halfmap
	 *  because player 2 has to wait for player 1 to submit halfmap first
	 */
	@Test 
	public void playerHasToWaitToSubmit() throws Exception {
		Game game = new Game();
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		Player player1 = new Player("john", "doe", "0123");
		Player player2 = new Player("jane", "doe", "0123");
		this.gameService.registerPlayer(game.getId(), player1);
		this.gameService.registerPlayer(game.getId(), player2);
		given(this.gameService.submitMap(game.getId(), player2.getId(), new NewMapNodes()))
		.willReturn(new ResponseEnvelope("NotYourTurn", "You should wait until other player submits halfmap"));
	}
	
	/**
	 * Player1 Successfully submits halfmap
	 */
	@Test
	public void submitHalfMapSuccess() throws Exception {
		Game game = new Game();
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		Player player1 = new Player("john", "doe", "0123");
		Player player2 = new Player("jane", "doe", "0123");
		this.gameService.registerPlayer(game.getId(), player1);
		this.gameService.registerPlayer(game.getId(), player2);
		List<NewMapNode> nmn = new ArrayList();
		for (int row = 0; row < 4; row++) {
			for (int col = 0 ; col < 8; col++) {
				NewMapNode node = new NewMapNode();
				node.setX(row);
				node.setY(col);;
				node.setTerrain(Terraintype.GRASS);
				nmn.add(node);
			}
		}
		nmn.get(0).setFortPresent(true);
		for(int i = 1 ; i < 5; i++) nmn.get(i).setTerrain(Terraintype.WATER);
		for(int i = 6 ; i < 10; i++) nmn.get(i).setTerrain(Terraintype.MOUNTAIN);
		Collections.shuffle(nmn);
		Collections.shuffle(nmn);
		NewMapNodes nmnodes = new NewMapNodes();
		nmnodes.setNewMapNode(nmn);
		given(this.gameService.submitMap(game.getId(), player1.getId(), nmnodes ))
		.willReturn(new ResponseEnvelope());
	}
	
	/**
	 * Player2 Successfully submits halfmap
	 */
	@Test
	public void submitHalfMap2Success() throws Exception {
		Game game = new Game();
		HashMap<String, Game> games = new HashMap();
		games.put(game.getId(), game);
		this.gameService.games = games;
		Player player1 = new Player("john", "doe", "0123");
		Player player2 = new Player("jane", "doe", "0123");
		this.gameService.registerPlayer(game.getId(), player1);
		this.gameService.registerPlayer(game.getId(), player2);
		List<NewMapNode> nmn = new ArrayList();
		for (int row = 0; row < 4; row++) {
			for (int col = 0 ; col < 8; col++) {
				NewMapNode node = new NewMapNode();
				node.setX(row);
				node.setY(col);;
				node.setTerrain(Terraintype.GRASS);
				nmn.add(node);
			}
		}
		nmn.get(0).setFortPresent(true);
		for(int i = 1 ; i < 5; i++) nmn.get(i).setTerrain(Terraintype.WATER);
		for(int i = 6 ; i < 10; i++) nmn.get(i).setTerrain(Terraintype.MOUNTAIN);
		Collections.shuffle(nmn);
		Collections.shuffle(nmn);
		NewMapNodes nmnodes = new NewMapNodes();
		nmnodes.setNewMapNode(nmn);
		this.gameService.submitMap(game.getId(), player1.getId(), nmnodes );
		given(this.gameService.submitMap(game.getId(), player2.getId(), nmnodes ))
		.willReturn(new ResponseEnvelope());
	}
	
	
	
	
	

}
