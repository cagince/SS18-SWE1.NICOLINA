package server.controller;

import java.util.HashMap;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import messages.GameIdentifier;
import messages.ResponseEnvelope;
import messages.halfMapRegister.request.HalfMap;
import messages.playerRegister.request.PlayerRegistration;
import server.model.Game;
import server.model.Player;
import server.service.GameService;
import server.service.PlayerService;

@RestController
@RequestMapping("/game")
public class GameController {
	
	
	@Autowired
	GameService gameService;
	
	@Autowired
	PlayerService playerService;
	
	
	private static final Logger logger = Logger.getLogger(GameController.class.getName());
	
	
	// „http://<domain>:<port>/game/new“ 
	@GetMapping(value="/new", produces = {"application/xml"})
	public GameIdentifier createNewGame() {
		logger.info("Creating new Game..");
		return new GameIdentifier(gameService.createGame().getId());
	}
	
	// http://<domain>:<port>/game/<SpielID>/register
    @PostMapping(value="/{game_id}/register", consumes= {"application/xml"}, produces= {"application/xml"})
	public ResponseEnvelope registerNewPlayer(@PathVariable final String game_id, @RequestBody PlayerRegistration reg) {
    	
    	logger.info("Register player request to: " + game_id);
    	return gameService.registerPlayer(game_id, playerService.createPlayer(reg.getStudentFirstName(), reg.getStudentLastName(), reg.getStudentID()));
	}
    
    
    //„http://<domain>:<port>/game/<SpielID>/halfmap“
    @PostMapping(value="/{game_id}/halfmap")
	public ResponseEnvelope submitHalfMap(@PathVariable final String game_id, @RequestBody HalfMap req ) { 
    	logger.info("HalfMap Submission to: " + game_id);
    	return  gameService.submitMap(game_id, req.getUniquePlayerID(), req.getNewMapNodes());
    }
    
    // „http://<domain>:<port>/game/<SpielID>/state/<SpielerID>“
    @GetMapping(value="/{game_id}/state/{player_id}")
    public ResponseEntity getGameStatus(@PathVariable final String game_id, @PathVariable final String player_id) {
    	logger.info("Game Status asked for: " + game_id + " by " + player_id);
    	return null;
    }
    
    
    // „http://<domain>:<port>/game/<SpielID>/move“
    @PostMapping(value="/{game_id}/move")
    public ResponseEntity submitMove(@PathVariable final String game_id) {
    	logger.info("Move has been registered for: " + game_id);
    	return null;
    }
    
    
	
}
