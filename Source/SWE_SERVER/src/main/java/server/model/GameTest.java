package server.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import messages.halfMapRegister.request.Terraintype;
import messages.halfMapRegister.request.HalfMap.NewMapNodes;
import messages.halfMapRegister.request.HalfMap.NewMapNodes.NewMapNode;

public class GameTest {
	
	
	public Terraintype getType(char c) {
		switch(c) {
		case 'G':
			return Terraintype.GRASS;
		case 'W':
			return Terraintype.WATER;
		case 'M':
			return Terraintype.MOUNTAIN;
		default:
			return Terraintype.GRASS;
		}
	}
	
	// generates NewMapNodes
	public NewMapNodes getNodes() {
		List<NewMapNode> nmn = new ArrayList();
		
		char[] ch = new char[] {
				'G', 'G', 'G', 'G', 'M', 'M', 'M', 'M',
				'M', 'M', 'G', 'G', 'G', 'G', 'G', 'G',
				'G', 'G', 'G', 'G', 'G', 'W', 'W', 'G',
				'W', 'G', 'W', 'W', 'G', 'G', 'G', 'G'};
		
		int row = 0;
		int col = 0;
		for ( char c : ch ) {
			NewMapNode node = new NewMapNode();
			System.out.println(row + " - " + col + " : " + c);
			node.setX(col++);
			node.setY(row);
			node.setTerrain(getType(c));
			nmn.add(node);
			if (col == 8) {
				row++;
				col = 0;
			}
		}
		
		
		nmn.get(0).setFortPresent(true);
		Collections.shuffle(nmn);
		Collections.shuffle(nmn);
		NewMapNodes nmnodes = new NewMapNodes();
		nmnodes.setNewMapNode(nmn);
		char[][] arr = new char[8][4];
		for (NewMapNode n : nmnodes.getNewMapNode()) {
			System.out.println(n.getX() + " - " +  n.getY());
			arr[n.getX()][n.getY()] = n.getTerrain().name().charAt(0); // nur den 1. character
			
		}
		
		return nmnodes;
	}
	// tries to register 3 player to 1 game and fails
	@Test
	public void fails() {
		Game game = new  Game();
		boolean thrown = false;
		Player player1 = new Player("john", "doe", "1234");
		Player player2 = new Player("jane", "doe", "1234");
		Player player3 = new Player("mike", "doe", "1234");
		try {
			game.addPlayer(player1);
			game.addPlayer(player2);
			game.addPlayer(player3);
		} catch( Exception e) {
			thrown = true;
		}
		assertTrue(thrown);
	}


	// registers 2 players , generates halfmap and adds to game
	@Test
	public void passes() throws Exception {
		Game game = new  Game();
		Player player1 = new Player("john", "doe", "1234");
		Player player2 = new Player("jane", "doe", "1234");
		game.addPlayer(player1);
		game.addPlayer(player2);
		game.setHalfMap1(new HalfMap(getNodes()));
		game.setHalfMap2(new HalfMap(getNodes()));
		assertTrue(game.getHalfMap1()!= null && game.getHalfMap2() != null);
	}

}
