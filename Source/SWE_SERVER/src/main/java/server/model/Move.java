package server.model;

public class Move {
	
	
	private int new_x;
	private int new_y;
	
	
	
	public Move(int new_x, int new_y) throws Exception {
		this.new_x = new_x;
		this.new_y = new_y;
		this.validate();
	}
	
	
	
	// business rule #7 AI cannot move across the borders
	public void validate() throws Exception {
		if (new_x < 0 || new_x > 7 || new_y < 0 || new_y > 3) throw new Exception("AI cannot pass the borders of the map");
		
	}

}
