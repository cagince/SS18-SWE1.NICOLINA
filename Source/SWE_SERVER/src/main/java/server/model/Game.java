package server.model;

import java.util.UUID;

public class Game {
	
	private String id;
	
	private Player player1;
	private HalfMap halfMap1;
	private Player player2;
	private HalfMap halfMap2;
	
	private int round;
	private boolean p1Turn;
	
	
	
	
	public Game() {
		this.id = UUID.randomUUID().toString();
	}
	public void addPlayer(Player player) throws Exception {
		if (this.player1 == null) this.setPlayer1(player);
		else if (this.player2 == null) this.setPlayer2(player);
		else throw new Exception("game is already full");
	}
	
	/*
	 * ########## GETTERS AND SETTERS
	 */
	
	public Player getPlayer1() {
		return player1;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public HalfMap getHalfMap1() {
		return halfMap1;
	}

	public void setHalfMap1(HalfMap halfMap1) {
		this.halfMap1 = halfMap1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public HalfMap getHalfMap2() {
		return halfMap2;
	}

	public void setHalfMap2(HalfMap halfMap2) {
		this.halfMap2 = halfMap2;
	}

	// #Business logics
	
	// business rules #6 and #2
	public void validateMove(boolean is_p1, String move) throws Exception {
		// business rule #6 Player should wait for it's turn before submitting next move
		if ((is_p1 && !this.p1Turn) || (!is_p1 && this.p1Turn))  throw new Exception("User should wait for it's turn");
		
		// get the position of player
		int[] player_position = is_p1 ? this.player1.getPosition() : this.player2.getPosition(); // get position of the player
		int[] new_position = new int[2];
		
		// [ 3, 2 ] -> rechts -> [4,2] verändert x axe, 
		if (move == "Right") new_position = new int[]{player_position[0]+1,player_position[1]}; // x +1
		if (move == "Left") new_position = new int[]{player_position[0]-1,player_position[1]}; // x -1
		if (move == "Up") new_position = new int[]{player_position[0],player_position[1]-1}; // y + 1
		if (move == "Down") new_position = new int[]{player_position[0],player_position[1]+1}; // y - 1
		
		Move next_position = new Move(new_position[0], new_position[1]); // checks if move is in the map
		
		// business rule #2 player should not step on a water field
		if( is_p1 ? !this.halfMap1.isNewPositionValid(new_position[0], new_position[1]) : !this.halfMap2.isNewPositionValid(new_position[0], new_position[1])) throw new Exception("player cannot step on water");
	}
	
	// business rule #8 game cannot last longer than 200 rounds
	public void nextRound() throws Exception {
		this.round++;
		if (this.round > 200) throw new Exception("Game cannot last longer than 200 rounds");
	}
	
	
	// business rule #12 Player cannot be placed in an island
	public void validatePlayerPosition(boolean is_p1, int x, int y) throws Exception {
		char[][] halfmap = is_p1 ? this.halfMap1.getTerrains() : this.halfMap2.getTerrains();
		/**
		 *     W
		 *   W P W
		 *     W
		 */
		if(!((x != 0 && halfmap[x-1][y] != 'W') || 
			 (x != 7 && halfmap[x+1][y] != 'W') ||
			 (y != 0 && halfmap[x][y-1] != 'W') ||
			 (y != 3 && halfmap[x][y+1] != 'W'))
		  ) throw new Exception("Player cannot be in an island");
	}
	

}
