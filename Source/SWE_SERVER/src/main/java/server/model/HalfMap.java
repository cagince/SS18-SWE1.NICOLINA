package server.model;

import java.util.Random;

import messages.halfMapRegister.request.HalfMap.NewMapNodes;
import messages.halfMapRegister.request.HalfMap.NewMapNodes.NewMapNode;

public class HalfMap {
	
	private Integer id;
	
	/**
	 * wwgggmg
	 * gggww
	 */
	
	private char[][] terrains;
	private int[] fortCoordinates; // [x, y]
	private int[] tresureCoordinates; // [x, y] 
	
	private Game game;
	
	public HalfMap(NewMapNodes nodes) throws Exception {
		if (nodes.getNewMapNode().size() != 8*4) throw new Exception("Number of terrains are wrong");
		this.terrains = new char[8][4];
		this.fortCoordinates = new int[2];
		this.tresureCoordinates = new int[2];
		
		// for each MapNode -> 
		/**
		 * 
		 <newMapNode>
			<X>6</X> nach rechts
			<Y>3</Y> nach unten
			<fortPresent>false</fortPresent>
			<terrain>Water</terrain> 1. buchstabe
		</newMapNode>
		 */
		for(NewMapNode node : nodes.getNewMapNode()) {
			this.terrains[node.getX()][node.getY()] = node.getTerrain().name().charAt(0); // nur den 1. character
			if (node.isFortPresent()) { // falls den burg da ist
				this.fortCoordinates[0] = node.getX();
				this.fortCoordinates[1] = node.getY();
			}
		}
		setTresure();
		validate(); // validieren
	}
	
	// place the tresure in a grass field which is not the same as castle
	public void setTresure() {
		Random r = new Random();
		int x = r.nextInt(8);
		int y = r.nextInt(4);
		if (this.terrains[x][y] == 'G' && x != this.fortCoordinates[0] && y != this.fortCoordinates[1]) {
			this.tresureCoordinates[0] = x;
			this.tresureCoordinates[1] = y;
		} else {
			setTresure();
		}
	}
	
		
	public void validate() throws Exception {
		
		// business rule #1 8x4 for each haldmap
		int rows_length = 8;
		int cols_length = 4;
		if (this.terrains.length != rows_length || this.terrains[0].length != cols_length) throw new Exception("map should have 8 rows and 4 columns");
		
		// business rule #3 fort and tresure can only be in grass
		boolean is_fort_on_grass = this.terrains[this.fortCoordinates[0]][this.fortCoordinates[1]] == 'G';
		boolean is_tresure_on_grass = this.terrains[this.tresureCoordinates[0]][this.tresureCoordinates[1]] == 'G';
		if (!is_fort_on_grass || !is_tresure_on_grass) throw new Exception("fort and tresure should be on grass");
		
		// business rule #4 fort and castle cannot be in same place
		if (this.fortCoordinates[0] == this.tresureCoordinates[0] && this.fortCoordinates[1] == this.tresureCoordinates[1]) throw new Exception("Fort and tresure cannot be in the same place");
		
		// business rule #10 mind 3 mountains, 14 grass, 4 water
		int water_count = 0;
		int grass_count = 0;
		int mountain_count = 0;
		for (char[] row : this.terrains) {
			for (int field : row) {
				if (field == 'W') water_count++;
				if (field == 'G') grass_count++;
				if (field == 'M') mountain_count++;
			}
		}
		if (water_count < 4)throw new Exception("there has to be atleast 4 water fields");
		if (mountain_count < 3)throw new Exception("there has to be atleast 3 mountain fields");
		if (grass_count < 14)throw new Exception("there has to be atleast 14 grass fields");
		
		// business rule #11 borders are allowed to have max of 3 water fields
		int top_water_count = 0;
		int bot_water_count = 0;
		int left_water_count = 0;
		int right_water_count = 0;
		for (int i = 0; i < this.terrains.length; i++) {
			if (this.terrains[i][0] == 'W') left_water_count++;
			if (this.terrains[i][terrains[i].length -1] == 'W') right_water_count++;
		}
		for (int j = 0; j < this.terrains[0].length; j++) {
			if (this.terrains[0][j] == 'W') top_water_count++;
			if (this.terrains[terrains[0].length -1][j] == 'W') bot_water_count++;
		}
		if (top_water_count >= 3 || bot_water_count >= 3 || left_water_count >= 3 || right_water_count >=3) throw new Exception("Borders should have less than 3 water fields");
	}
	
	
	// business rule #2 player cannot step on water.
	public boolean isNewPositionValid(int x, int y) {
		return this.terrains[x][y] != 'W';
	}
	
	
	
	/*
	 * ########## GETTERS AND SETTERS
	 */
	



	public char[][] getTerrains() {
		return terrains;
	}




	
	
	

}
