package server.model;


import java.util.List;
import java.util.UUID;


public class Player {
	
	private String id;
	
	private String firstName;
	private String lastName;
	private String matrNr;
	
	private int[] position;
	
	public Player() {}
	
	public Player(String firstname, String lastname, String matrNr) {
		this.id = UUID.randomUUID().toString();
		this.firstName = firstname;
		this.lastName = lastname;
		this.matrNr = matrNr;
	}

	/*
	 * ########## GETTERS AND SETTERS
	 */

	public String getFirstName() {
		return firstName;
	}

	public String getId() {
		return id;
	}


	
	public int[] getPosition() { return this.position; }

}
