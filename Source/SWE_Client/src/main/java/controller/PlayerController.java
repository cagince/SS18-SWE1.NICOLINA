package controller;

import dataobjects.PlayerID;
import messages.HalfMapStruct;
import model.HalfMap;

// Controls the player actions
public class PlayerController {
	
	
	private PlayerID playerId;
	private HalfMap halfMap;
	
	
	private int x;
	private int y;
	
	
	
	public int getX() {
		return x;
	}




	public void setX(int x) {
		this.x = x;
	}




	public int getY() {
		return y;
	}




	public void setY(int y) {
		this.y = y;
	}




	// Constructor
	public PlayerController(PlayerID playerId) {
		this.playerId = playerId;
		this.halfMap = null;
	}
	
	
	
	
	public PlayerID getPlayerID() { return this.playerId; }
	
	
	
	public void generateHalfMap() {
		this.halfMap = new HalfMap();
	}
	
	
	
	public HalfMap getHalfMap() {
		return this.halfMap;
	}
	
	public void generateMove(int x , int y) {
		
		char[][] halfmap = this.halfMap.getCharMap();
		//for (int row = 0; row < halfmap.length; row++) {
		//	for( int col = 0 ; col < halfmap[row].length; col++) {
		//		
		//	}
		//}
	}
	
	
	// TODO::: finalize AI behaviour!
	public String getNextMove(int x, int y) {
		this.halfMap.getPossibleMoves(x, y);
		return "RIGHT";
	}
	
	
	
	
}
