package controller;

public class AIHelper {
	
	private char[][] halfmap;
	private boolean[][] trace;
	
	private boolean hasTreasure;
	private boolean canGoOtherSide;
	
	private int x;
	private int y;
	
	public AIHelper(char[][] halfmap, int x, int y) {
		this.halfmap = halfmap;
		this.trace = new boolean[halfmap.length][halfmap[0].length];
		this.canGoOtherSide = false;
		move(x,y);
	}
	
	// moves the avatar to given coordinates
	public void move(int x,int y) {
		this.x = x;
		this.y = y;
		this.trace[x][y] = true;
		if (this.halfmap[x][y] == 't') {
			this.hasTreasure = true;
			this.canGoOtherSide = true;
		}
	}
	
	// checks if avatar has been on the spot before
	public boolean didMove(int x, int y) { return this.trace[x][y]; }
	// checks if avatar can move to the direction
	public boolean canMove(String direction) {
		boolean is_valid = false;
		// TODO: set logic to define move
		switch(direction) {
			case "top":
				is_valid = x >= 1 && halfmap[x][y] == 'g';
				break;
			case "right":
				is_valid = y >= 1 && halfmap[x][y] == 'g';
				break;
			case "bottom":
				is_valid = x >= 1 && halfmap[x][y] == 'g';
				break;
			case "left":
				is_valid = y >= 1 && halfmap[x][y] == 'g';
				break;
			default:
				is_valid = false;
		}
		return is_valid;
	}
	
	public void lookAround() {
		boolean top = canMove("top");
		boolean right = canMove("right");
		boolean bottom = canMove("bottom");
		boolean left = canMove("left");
	}
	

}
