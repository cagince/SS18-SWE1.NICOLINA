package controller;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import dataobjects.GameID;
import dataobjects.PlayerID;
import messages.gameStatus.GameState;
import messages.gameStatus.MapNode;
import messages.gameStatus.MapNodes;
import messages.gameStatus.Player;
import messages.gameStatus.PlayerGameStatevalues;
import messages.gameStatus.PlayerStatevalues;
import messages.gameStatus.Players;
import messages.gameStatus.ResponseEnvelope;
import messages.halfMapRegister.request.HalfMap;
import messages.halfMapRegister.request.HalfMap.NewMapNodes;
import messages.halfMapRegister.request.HalfMap.NewMapNodes.NewMapNode;




// Controls The Entire Game Logic. Decides if finished or not...etc.
public class GameController {
	
	private GameID gameId;
	private PlayerController playerController;
	
	private URI uri;
	
	private GameState state;
	
	private boolean isOwnTurn;
	
	
	
	private static final Logger logger = Logger.getLogger(GameController.class.getName());
	// Constructor
	public GameController(GameID gameId) {
		this.gameId = gameId;
		logger.info("Game has been Added: " + gameId.getId());
	}
	
	
	public void setUri(URI uri) {
		this.uri = uri;
	}

	public GameID getGameId() {
		return gameId;
	}

	public void setGameId(GameID gameId) {
		this.gameId = gameId;
	}

	public void setPlayerId(PlayerID playerId) {
		this.playerController = new PlayerController(playerId);
		logger.info("New Player has been added to game: " +  playerId.getId());
	}
	
	public PlayerController getPlayer() {
		return this.playerController;
	}
	
	public void initializeURI(URL baseURL) throws Exception {
		this.uri = new URL(baseURL, "game/" + this.gameId.getId()+ "/state/" + this.playerController.getPlayerID().getId()).toURI();
		System.out.println(this.uri.toString());
	}
	
	// Sets game Status
	public void setGameStatus(GameState state) {
		this.state = state;
	}
	
	// sends a request to server and initializes the State with data.
	public void updateState() {
		RestTemplate tmp = new RestTemplate();
		ResponseEnvelope res = tmp.getForObject(this.uri, ResponseEnvelope.class);
		this.state = res.getData();
	}
	
	// waits until IS_YOUR_TOURN and then generates the halfmap.
	public boolean initializeGame() {
		if (this.waitForTurn()) {
			this.playerController.generateHalfMap();
		}
		return true;
	}
	
	
	
	// returns RegisterHalfmapRequest and prints the halfmap while doing it
	public HalfMap getHalfMapPayload() {
		NewMapNodes nmn = new NewMapNodes();
		List<NewMapNode> newMapNodes = this.playerController.getHalfMap().getNewMapNodes();
		//for (NewMapNode n : newMapNodes) {
		//	System.out.println(n.getX() + " x " + n.getY());
		//}
		logger.info("Half Map Has been Generated");
		System.out.println("###############################");
		for (NewMapNode node: newMapNodes) {
			if (node.getY() == 0) {
				System.out.print("\n");
			}
			System.out.print(node.getTerrain().name().charAt(0) + " ");
		}
		System.out.print("\n");
		System.out.println("###############################");
		nmn.setNewMapNode(newMapNodes);
		HalfMap hmReq = new HalfMap();
		hmReq.setNewMapNodes(nmn);
		hmReq.setUniquePlayerID(this.playerController.getPlayerID().getId());
		return hmReq;
		
	}
	
	
	// Waits for players turn to come
	public boolean waitForTurn() {
		this.updateState();
		while(!isOwnTurn()) {
			updateState();
		}
		
		
	/*	
		List<MapNode> mn = this.state.getMap().getMapNodes().getMapNode();
		char[][] map = new char[8][8];
		for (MapNode n : mn) {
			map[n.getX()][n.getY()] = n.getTerrain().value().charAt(0);
			//if (n.getFortState().) obj += "&";
			//if (n.getPlayerPositionState().equals(PlayerStatevalues.ENEMY_PLAYER_POSITION)) obj += "&";
			//if (n.getPlayerPositionState().equals(PlayerStatevalues.MY_POSITION)) obj += "!";
			//if (n.getPlayerPositionState().equals(PlayerStatevalues.BOTH_PLAYER_POSITION)) obj += "$";
		}
		for(char[] r : map ) {
			for(char[] c : map ) {
				System.out.print(c + " ");
			}
			System.out.println(" ");
		}
		
	*/	
		
		this.isOwnTurn = true;
		return true;
		
	}
	
	// Returns true if finished
	public boolean isFinished()  {
		for (Player p : this.state.getPlayers().getPlayer()) {
			if (p.getUniquePlayerID().equals(this.playerController.getPlayerID().getId())) {
				logger.info("Game Status update: " + p.getState().toString());
				return p.getState().equals(PlayerGameStatevalues.WON) || p.getState().equals(PlayerGameStatevalues.LOST);
			}
		}
		return false;
	}
	
	
	// Returns true if user should_act_next false otherwise
	public boolean shouldActNext() {
		Player p1 = this.state
			.getPlayers()
			.getPlayer()
			.stream()
			.filter(p -> p.getUniquePlayerID().equals(this.playerController.getPlayerID().getId()))
			.findFirst()
			.get();
		boolean should_act = p1.getState().equals(PlayerGameStatevalues.SHOULD_ACT_NEXT);
		if (should_act) {
			logger.info("Next Move is Mine!");
		} else {
			logger.info("Waiting for other player to make a move...");
		}
		return should_act;
	}
	
	
	// Returns true if user should_act_next false otherwise.
	public boolean isOwnTurn() {
		
		boolean should_act = false;
		for (Player p : this.state.getPlayers().getPlayer()) {
			if (p.getUniquePlayerID().equals(this.playerController.getPlayerID().getId())) {
				should_act = p.getState().equals(PlayerGameStatevalues.SHOULD_ACT_NEXT);
			}
		}
		if (should_act) {
			logger.info("Next Move is Mine!");
		} else {
			logger.info("Waiting for Turn");
		}
		return should_act;
	}
	
	
	
	
	
	
 
}
