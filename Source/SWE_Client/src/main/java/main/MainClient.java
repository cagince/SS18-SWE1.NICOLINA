package main;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Logger;

import org.springframework.web.client.RestClientException;

import controller.GameController;
import controller.PlayerController;
import dataobjects.GameID;
import dataobjects.PlayerID;
import dataobjects.PlayerInformation;
import dataobjects.PlayerInformation2;
import messages.GameIdentifier;
import messages.avatarMove.request.AvatarMove;
import messages.avatarMove.request.MoveValues;
import network.ServerConnection;
import view.CLI;

public class MainClient {
	
	
	
	private static final Logger logger = Logger.getLogger(MainClient.class.getName());
	
	

	public static void main(String[] args) {
		
		String url = "http://swe.wst.univie.ac.at:18235";
		String game_id = null;
		
		URL baseServerURL = null;
		try {
			baseServerURL = new URL("http://swe.wst.univie.ac.at:18235");
		} catch (MalformedURLException e) {
			logger.warning("Server Url is malformed check inputs for more information!");
			e.printStackTrace();
		}
		//URL baseServerURL = new URL(args.length >= 2 ? args[1] : "http://swe.wst.univie.ac.at:18235");
		GameController gameController = null;
		//GameController gameController = new GameController(args.length >= 3  ? new GameID(new GameIdentifier(args[2])) : server.createNewGame());
		ServerConnection server = null;
		
		CLI cli = new CLI();
		if (args.length == 3) {
			// arguments are given in -jar
			url = args[1];
			game_id = args[2];
		} else {
			// no arguments
			int response = cli.start();
			if (response == 1 || response == 0) {
				game_id = response == 1 ? cli.getGameID() : null;
			} else {
				logger.info("not a valid option");
				return;
			}
		}
		
		// initialize server
		server = new ServerConnection(baseServerURL);
		logger.info("Server Connection has been established to: " + baseServerURL);
		
		// initialize game && game Controller
		if (game_id == null) {
			try {
				gameController = new GameController(server.createNewGame());
			} catch (Exception e) {
				logger.warning("Error occured while creating new Game");
				e.printStackTrace();
			}
		} else {
			gameController = new GameController(new GameID(new GameIdentifier(game_id)));
		}
		PlayerController player = null;
		
		// RegisterPlayer
		try {
			player = initializePlayer(gameController, server);
		} catch (Exception e) {
			logger.warning("Unable to initialize player");
			e.printStackTrace();
		}
		
		
		try {
			// Start Game
			gameController.initializeURI(baseServerURL);
			startGame(gameController, server, player);
		} catch (Exception e) {
			logger.warning("Unable to start the game");
			e.printStackTrace();
		}

	}
	
	
	static void startGame(GameController gc, ServerConnection server, PlayerController player) throws Exception {
		// init Game
		initializeGame(gc, server, player);

		boolean finished = false;
		while(!finished) {
			if(gc.shouldActNext()) {
				AvatarMove payload = new AvatarMove();
				payload.setMove(MoveValues.fromValue(player.getNextMove(0,1)));
				payload.setUniquePlayerID(player.getPlayerID().getId());
				server.submitMove(gc.getGameId(), payload);
				finished = gc.isFinished();
			} else {
				System.out.println("shouldn't");
			}
		}
	}
	
	
	static PlayerController initializePlayer(GameController gc, ServerConnection server) throws Exception {
		// initialize controller with game id
		// register player
		PlayerID pid = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
		logger.info("registered player: " + pid.getId());
		gc.setPlayerId(pid);
		PlayerController player = gc.getPlayer();
		return player;
	}
	
	static void initializeGame(GameController gc, ServerConnection server, PlayerController player) throws Exception {
		// if the game is initialized succesfully submit halfmap
		if (gc.initializeGame()) {
			while(!server.AddHalfMap(gc.getGameId(), gc.getHalfMapPayload())) {
				player.generateHalfMap();
				server.AddHalfMap(gc.getGameId(), gc.getHalfMapPayload());
			}
		}
	}
	
}
