package dataobjects;

public class PlayerInformation {

	//default information
	private final static String matNr = "01063026";
	private final static String firstName = "Nicolina"; 
	private final static String lastName = "Kozul";
	
	public static String getMatnr() {
		return matNr;
	}
	public static String getFirstname() {
		return firstName;
	}
	public static String getLastname() {
		return lastName;
	}
	
	public String toString() {
		return "Name: " + firstName + " "  + lastName + " MatNr: " + matNr;
	}
	
}
