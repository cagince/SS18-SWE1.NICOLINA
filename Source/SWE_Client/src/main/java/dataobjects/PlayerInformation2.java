package dataobjects;

public class PlayerInformation2 {

	//default information
	private final static String matNr = "00929254";
	private final static String firstName = "cagin"; 
	private final static String lastName = "cecen";
	
	public static String getMatnr() {
		return matNr;
	}
	public static String getFirstname() {
		return firstName;
	}
	public static String getLastname() {
		return lastName;
	}
	
	public String toString() {
		return "Name: " + firstName + " "  + lastName + " MatNr: " + matNr;
	}
	
}
