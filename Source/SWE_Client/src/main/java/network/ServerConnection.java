package network;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Logger;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import controller.PlayerController;
import dataobjects.GameID;
import dataobjects.PlayerID;
import dataobjects.PlayerInformation;
import dataobjects.PlayerInformation2;
import dataobjects.UniqueID;

import messages.GameIdentifier;
import messages.HalfMapStruct;
import messages.PlayerIdentifier;
import messages.RegisterPlayer;
import messages.ResponseEnvelope;
import messages.ResponseState;
import messages.avatarMove.request.AvatarMove;
import messages.gameStatus.GameState;
import messages.halfMapRegister.request.HalfMap;

public class ServerConnection {
	
	private static final Logger logger = Logger.getLogger(ServerConnection.class.getName());

	private final URL baseUrl;
	
	public ServerConnection(URL baseUrl)
	{
		this.baseUrl = baseUrl;
	}
	
	public URL getBaseUrl() { return this.baseUrl;}
	
	public GameID createNewGame() throws Exception {
		
		URL gameNewUrl = new URL(baseUrl, "game/new"); // java.net
		RestTemplate restTemplate = new RestTemplate(); // spring.web
		GameIdentifier gameIdentifierMessage = restTemplate.getForObject(gameNewUrl.toURI(), GameIdentifier.class);
		
				 
		return new GameID(gameIdentifierMessage);
	}
	
	
	public PlayerID registerNewPlayer(GameID gameID, PlayerInformation playerInfo) throws Exception
	{
		RegisterPlayer payload = new RegisterPlayer(playerInfo.getMatnr(), playerInfo.getFirstname(), playerInfo.getLastname());
		URL registerPlayerUrl = new URL(baseUrl, "game/"+gameID.getId()+"/register");
		
		logger.info("Registerin created a register player request:" + registerPlayerUrl.toURI().toString());
		RestTemplate restTemplate = new RestTemplate();
		ResponseEnvelope<PlayerIdentifier> playerIdMessage = restTemplate.postForObject(registerPlayerUrl.toURI(), payload, ResponseEnvelope.class);
		return new PlayerID(playerIdMessage.getData());
	}
	
	public PlayerID registerNewPlayer2(GameID gameID, PlayerInformation2 playerInfo) throws Exception
	{
		RegisterPlayer payload = new RegisterPlayer(playerInfo.getMatnr(), playerInfo.getFirstname(), playerInfo.getLastname());
		URL registerPlayerUrl = new URL(baseUrl, "game/"+gameID.getId()+"/register");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEnvelope<PlayerIdentifier> playerIdMessage = restTemplate.postForObject(registerPlayerUrl.toURI(), payload, ResponseEnvelope.class);
		return new PlayerID(playerIdMessage.getData());
	}
	
	
	public boolean AddHalfMap(GameID gameID, HalfMap payload) throws Exception {
		
		URL addHalfMapUrl = new URL(baseUrl, "game/"+ gameID.getId()+"/halfmap");
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEnvelope res = restTemplate.postForObject(addHalfMapUrl.toURI(), payload, ResponseEnvelope.class);
		boolean isOk = res.isOk();
		if(isOk) logger.info("Halfmap added succesfully");
		return isOk;

		//ResponseEnvelope<ResponseState> halfMapMessage = restTemplate.postForObject(addHalfMapUrl.toURI(), payload, ResponseEnvelope.class);
		//System.out.println(halfMapMessage.getData());
	}
	

	public boolean submitMove(GameID gameID,AvatarMove payload) throws Exception {
		URL addMoveUrl = new URL(baseUrl, "game/"+ gameID.getId()+"/move");
		RestTemplate tmp = new RestTemplate();
		ResponseEnvelope res = tmp.postForObject(addMoveUrl.toURI(), payload, ResponseEnvelope.class);
		
		return res.isOk();
		
		
	}
}
