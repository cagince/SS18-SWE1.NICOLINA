package test;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;
import org.junit.Before;

import controller.GameController;
import dataobjects.PlayerID;
import dataobjects.PlayerInformation;
import dataobjects.PlayerInformation2;
import network.ServerConnection;

public class GameControllerTest {
	
	String baseUrl = "http://swe.wst.univie.ac.at:18235";
	

	@Test
	public void testIsFinished() throws Exception {
		URL baseURL = new URL(baseUrl);
		ServerConnection server = new ServerConnection(baseURL);
		GameController gc = new GameController(server.createNewGame());
		PlayerID pid1 = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
		gc.setPlayerId(pid1);
		PlayerID pid2 = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
		gc.initializeURI(baseURL);		
		boolean initialized = gc.initializeGame();
		boolean is_finished = gc.isFinished();
		System.out.println(is_finished);
		assertTrue(!is_finished);
	}

	@Test
	public void testShouldActNext() throws Exception {
		URL baseURL = new URL(baseUrl);
		ServerConnection server = new ServerConnection(baseURL);
		GameController gc = new GameController(server.createNewGame());
		PlayerID pid1 = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
		gc.setPlayerId(pid1);
		PlayerID pid2 = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
		gc.initializeURI(baseURL);		
		boolean initialized = gc.initializeGame();
		boolean should_act= gc.shouldActNext();
		assertTrue(should_act);
	}

	@Test
	public void testIsOwnTurn() throws Exception {
		URL baseURL = new URL(baseUrl);
		ServerConnection server = new ServerConnection(baseURL);
		GameController gc = new GameController(server.createNewGame());
		PlayerID pid1 = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
		gc.setPlayerId(pid1);
		PlayerID pid2 = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
		gc.initializeURI(baseURL);		
		boolean initialized = gc.initializeGame();
		boolean is_own = gc.isOwnTurn();
		assertTrue(is_own);
	}

}
