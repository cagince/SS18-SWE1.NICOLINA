/**
 * 
 */
package test;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Test;

import controller.GameController;
import controller.PlayerController;
import dataobjects.GameID;
import dataobjects.PlayerID;
import dataobjects.PlayerInformation;
import dataobjects.PlayerInformation2;
import messages.avatarMove.request.AvatarMove;
import messages.avatarMove.request.MoveValues;
import network.ServerConnection;

public class ServerConnectionTest {
	
	
	
	String baseUrl = "http://swe.wst.univie.ac.at:18235";
	ServerConnection server;
	
	

	/**
	 * Test method for {@link network.ServerConnection#ServerConnection(java.net.URL)}.
	 */
	@Test
	public void testServerConnection() {
		try {
			ServerConnection server = new ServerConnection(new URL(baseUrl));
			assertEquals(server.getBaseUrl().toString(), baseUrl);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			fail("Cannot initialize server");
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link network.ServerConnection#createNewGame()}.
	 * @throws Exception 
	 */
	@Test
	public void testCreateNewGame() throws Exception {
		ServerConnection server = new ServerConnection(new URL(baseUrl));
		GameID gid = server.createNewGame();
		assertTrue(gid.getId().getClass().equals(String.class));
	}

	/**
	 * Test method for {@link network.ServerConnection#registerNewPlayer(dataobjects.GameID, dataobjects.PlayerInformation)}.
	 * @throws Exception 
	 */
	@Test
	public void testRegisterNewPlayer() {
		ServerConnection server;
		try {
			server = new ServerConnection(new URL(baseUrl));
			GameController gc = new GameController(server.createNewGame());
			PlayerID pid = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
			assertTrue(pid.getId().getClass().equals(String.class));
		} catch (Exception e) {
			fail("Cannot register player");
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link network.ServerConnection#registerNewPlayer2(dataobjects.GameID, dataobjects.PlayerInformation2)}.
	 * @throws MalformedURLException 
	 */
	@Test
	public void testRegisterNewPlayer2() {
		ServerConnection server;
		try {
			server = new ServerConnection(new URL(baseUrl));
			GameController gc = new GameController(server.createNewGame());
			PlayerID pid = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
			assertTrue(pid.getId().getClass().equals(String.class));
		} catch (Exception e) {
			fail("Cannot register player2");
			e.printStackTrace();
		}
	}
	
	
/**	
	@Test void initializeGame() throws Exception {
		ServerConnection server = new ServerConnection(new URL(baseUrl));
		GameController gc = new GameController(server.createNewGame());
		PlayerID pid1 = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
		PlayerID pid2 = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
		gc.initializeURI(new URL(baseUrl));
		boolean initialized = gc.initializeGame();
		assertTrue(initialized);
	}
**/
	/**
	 * Test method for {@link network.ServerConnection#AddHalfMap(dataobjects.GameID, messages.halfMapRegister.request.HalfMap)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddHalfMap() {
		
		URL baseURL;
		try {
			baseURL = new URL(baseUrl);
			ServerConnection server = new ServerConnection(baseURL);
			GameController gc = new GameController(server.createNewGame());
			PlayerID pid1 = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
			gc.setPlayerId(pid1);
			PlayerID pid2 = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
			gc.initializeURI(baseURL);
			boolean initialized = gc.initializeGame();
			boolean mapAdded = false;
			if (initialized) {
				mapAdded = server.AddHalfMap(gc.getGameId(), gc.getHalfMapPayload());
			}
			assertTrue(mapAdded);
		} catch (Exception e) {
			fail("Cannot add Halfmap");
			e.printStackTrace();
		}

	}


	/**
	 * Test method for {@link network.ServerConnection#submitMove(dataobjects.GameID, messages.avatarMove.request.AvatarMove)}.
	 * @throws MalformedURLException 
	 */
	@Test
	public void testSubmitMove() {
		try {
			
			URL baseURL = new URL(baseUrl);
			ServerConnection server = new ServerConnection(baseURL);
			GameController gc = new GameController(server.createNewGame());
			PlayerID pid1 = server.registerNewPlayer(gc.getGameId(), new PlayerInformation());
			gc.setPlayerId(pid1);
			PlayerID pid2 = server.registerNewPlayer2(gc.getGameId(), new PlayerInformation2());
			gc.initializeURI(baseURL);
			boolean initialized = gc.initializeGame();
			boolean mapAdded = false;
			if (initialized) {
				mapAdded = server.AddHalfMap(gc.getGameId(), gc.getHalfMapPayload());
			}
			PlayerController player = gc.getPlayer();
			AvatarMove payload = new AvatarMove();
			payload.setMove(MoveValues.fromValue(player.getNextMove(0,1)));
			payload.setUniquePlayerID(player.getPlayerID().getId());
			boolean is_ok = server.submitMove(gc.getGameId(), payload);
			assertFalse(is_ok);
		} catch (Exception e) {
			fail("Cannot submit move");
			e.printStackTrace();
			
		}
		
		
	}
	
	


}
