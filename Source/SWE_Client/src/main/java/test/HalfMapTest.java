package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import messages.gameStatus.MapNode;
import model.HalfMap;

public class HalfMapTest {

	@Test
	public void test() {
		HalfMap hm = new HalfMap();
		List<MapNode> nodes = hm.getMapNodes();
		assertTrue(nodes.size() == 32);
	}

}
