package model;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import messages.newMapNode;
import messages.gameStatus.MapNode;
import messages.halfMapRegister.request.HalfMap.NewMapNodes.NewMapNode;

// Handles the data about a Territory which is a 1x1 grid.
public class Territory {
	
	// Properties
	private int x;
	private int y;
	private String terrain;
	private boolean fortPresent;
	private int player;
	
	
	private static final Logger logger = Logger.getLogger(Territory.class.getName());
	
	// Constructor
	// initializes Territory
	public Territory(int x, int y, char terrain) {
		
		HashMap<Character, String> tMap = new HashMap<Character, String>();
		tMap.put('w', "Water");
		tMap.put('g', "Grass");
		tMap.put('m', "Mountain");
		this.player = 0;
		
		if (tMap.containsKey(terrain)) {
			this.x = x;
			this.y = y;
			this.terrain = tMap.get(terrain);
			this.fortPresent = false;
		} else {
			logger.log(Level.SEVERE, "TYPE IS NOT DEFINED", new Error("Cannot initialize terrain"));
		}
	}
	

	
	// Setters
	public void setFort() { this.fortPresent = true; }
	
	
	// returns a new Map Node
	public NewMapNode toNewMapNode() {
		return new NewMapNode(this.x, this.y, this.fortPresent, this.terrain);
	}
	// returns a MapNode
	public MapNode toMapNode() {
		return new MapNode(x,y,this.terrain, false, this.fortPresent);
	}
	
	// CLI methods
	public char echo() { return this.fortPresent ? 'c' : Character.toLowerCase(this.terrain.charAt(0)); }
	
	
	
}
