package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import messages.newMapNode;
import messages.gameStatus.MapNode;
import messages.halfMapRegister.request.HalfMap.NewMapNodes.NewMapNode;

public class HalfMap {
	
    private char[][] halfmap;
    
    private Territory[] halfmapTerr;

    private int rows;
    private int hmCX;
    private int hmCY;
    private int hmTX;
    private int hmTY;
    
    
	private static final Logger logger = Logger.getLogger(HalfMap.class.getName());
	
    public HalfMap() {
    	this.halfmapTerr = new Territory[32];
    	this.halfmap = populateHalfMap();
    	placeCastle();
    	placeTreasure();
    	
    	//printHalfMap();
    	
    }
    
    public void printHalfMap() {
    	for( int i = 0 ; i < this.halfmapTerr.length; i++) {
    		if (i%8 == 0) System.out.print("\n");
    		System.out.print(this.halfmapTerr[i].echo());
    	}
    }
    
    
    public boolean[] getPossibleMoves(int x, int y) {
    	
    	boolean top = y <4;
    	boolean bottom = y >= 0;
    	boolean left = y >= 0;
    	boolean right = y < 7;
    	
    	return new boolean[]{top, right, bottom, left};
    }
    
    
    // populates the halfmap with allowed number of water, grass and mountains
    private char[][] populateHalfMap() {
    	Random random = new Random();
    	char[] arr = new char[8*4];
    	int index = 0;
    	
    	
    	// Set the count of waters and mountains
    	int mountainCount = random.nextInt((5-4) +1 ) +4;
    	int waterCount = random.nextInt((5-4) +1) +4;
    	
    	
    	// set decided number of mountains and water and fill the rest with grass
    	for(int i = 0 ; i < mountainCount; i++) { arr[index++] = 'm';}
    	for(int i = 0 ; i < waterCount; i++) { arr[index++] = 'w';}
    	while (index < arr.length) { arr[index++] = 'g'; }
    	
    	// shuffle array
    	arr = shuffleArray(shuffleArray(arr));
    	return transformInto2DArray(arr);
    	
    }
    
    
    // Shuffles the given array
    private char[] shuffleArray(char[] array) {
    	
    	char tmp;
    	int index;
    	
    	Random random = new Random();
    	for( int i = array.length -1; i > 0 ; i--) {
    		index = random.nextInt(i +1);
    		tmp = array[index];
    		array[index]  = array[i];
    		array[i] = tmp;
    	}
    	return array;
    }
    
    // Transforms the given array into 2d array (4x8)
    private char[][] transformInto2DArray(char[] halfmap) {
    	
    	char[][] map = new char[4][8];
    	int lIndex = 0;
    	for (int col = 0 ; col < 8; col++) {
    		for (int row = 0; row < 4; row++) {
    			map[row][col] = halfmap[lIndex];
    			this.halfmapTerr[lIndex] = new Territory(col, row, halfmap[lIndex++]);
    		}
    	}
    	return map;
    }
    
    
    // Places Castle to Map
    private void placeCastle() {
    	Random random = new Random();
    	int row = 0;
    	int col = random.nextInt(8);
    	if(isValid('g',row, col) || isValid('b', row, col)) {
    		this.hmCX = row;
    		this.hmCY = col;
    		// this.halfmap[row][col] = 'c';
    		this.halfmapTerr[row*8+col].setFort();
    	} else {
    		placeCastle();
    	}
    }
    
    // places Treasure to map
    private void placeTreasure() {
    	Random random = new Random();
    	int row = random.nextInt(4);
    	int col = random.nextInt(8);
    	if (isValid('g', row, col)) {
    		if (this.hmCX != row || this.hmCY != col) {
    			this.hmTX = row;
    			this.hmTY = col;
    			// this.halfmap[row][col] = 't';
    		} else {
    			placeTreasure();
    		}
    	} else {
    		placeTreasure();
    	}
    	
    	
    }
    
    // checks if given coordinates are equal to type given
    private boolean isValid(char type, int row, int col) {
    	return this.halfmap[row][col] == type;
    }
    
    
    // Maps the 2d array to a 1d Territory array
    private void mapToTerritoryArray() {
    	this.halfmapTerr = new Territory[8*4];
    	int index = 0;
    	for (int i = 0 ; i < this.halfmap.length; i++) {
    		for (int j = 0 ; j < this.halfmap[i].length; j++) {
    			this.halfmapTerr[index++] = new Territory(i, j, this.halfmap[i][j]);
    		}
    	}
    }
    
    public List<NewMapNode> getNewMapNodes() {
    	
    	List<NewMapNode> nodes = new ArrayList();
    	for (Territory t : this.halfmapTerr) {
    		nodes.add(t.toNewMapNode());
    	}
    	return nodes;
    }
    
    public List<MapNode> getMapNodes() {
    	List<MapNode> nodes = new ArrayList();
    	for (Territory t : this.halfmapTerr) {
    		nodes.add(t.toMapNode());
    	}
    	return nodes;
    }
    
    
    public char[][] getCharMap() { return this.halfmap; }


}
