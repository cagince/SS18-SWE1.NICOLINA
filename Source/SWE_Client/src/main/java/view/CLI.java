package view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import messages.gameStatus.FortStatevalues;
import messages.gameStatus.MapNode;
import messages.gameStatus.MapNodes;
import messages.gameStatus.PlayerStatevalues;
import messages.gameStatus.TresureStatevalues;

public class CLI {
	
	
	
	static final Scanner sc = new Scanner(System.in);
	
	
	
	
	
	final String[] options = {"Start a new Game", "Join a game" };
	
	List<MapNode> nodes;
	String[][] map;
	
	// Constructor
	public CLI() {}
	
	
	public void setNodes(List<MapNode> nodes) {
		this.nodes = nodes;
	}
	
	
	// Maps the field types to map.
	private void initializeMap() {
		Map<String, String> chars = new HashMap();
		
		chars.put("Water", "~");
		chars.put("Grass", ".");
		chars.put("Mountain", "^");
		chars.put("p1", "$");
		chars.put("p2", "&");
		chars.put("fort", "#");
		chars.put("tresure", "*");
		
		
		for(MapNode node : nodes) {
			String terr = "";
			terr += chars.get(node.getTerrain().value());
			
			PlayerStatevalues playerState = node.getPlayerPositionState();
			
			boolean hasTresure = node.getFortState().equals(TresureStatevalues.MY_TRESURE_IS_PLACED_HERE);
			boolean hasFort = node.getFortState().equals(FortStatevalues.ENEMY_FORT_PRESENT) || node.getFortState().equals(FortStatevalues.MY_FORT_PRESENT);
			
			if(playerState.equals(PlayerStatevalues.NO_PLAYER_PRESENT)) {
				terr += chars.get(node.getTerrain().value());
				terr += chars.get(node.getTerrain().value());
				terr += chars.get(node.getTerrain().value());
			} else if(playerState.equals(PlayerStatevalues.MY_POSITION)) {
				terr += chars.get("p1");
				terr += hasFort ? chars.get("fort") : hasTresure ? chars.get("tresure") : chars.get("p1");
				terr += chars.get("p1");
				
			} else if(playerState.equals(PlayerStatevalues.ENEMY_PLAYER_POSITION)) {
				terr += chars.get("p2");
				terr += hasFort ? chars.get("fort") : hasTresure ? chars.get("tresure") : chars.get("p1");
				terr += chars.get("p2");
			} else if(playerState.equals(PlayerStatevalues.BOTH_PLAYER_POSITION)) {
				terr += chars.get("p1");
				terr += hasFort ? chars.get("fort") : hasTresure ? chars.get("tresure") : chars.get("p1");
				terr += chars.get("p2");
			}
			terr += chars.get(node.getTerrain().value());
			
			this.map[node.getX()][node.getY()] = terr;
		}
		
		
	}
	
	
	// print function
	private void print(String toPrint) {
		System.out.println(toPrint);
	}
	
	// checks if the option is valid or not
	private boolean isValidOption(int option, int max) {
		return option < max && option >= 0;
	}
	
	
	// asks what to do at the beginning;
	// new game or join game
	public int start() {
		print("Please select an option");
		for (int i = 0 ; i < this.options.length; i++) {
			print("please press " + i + " to " + this.options[i]);
		}
		print("#########################");
		
		int task = sc.nextInt();
		if (isValidOption(task, this.options.length)) {
			print(options[task] + " has been selected");
		}
		return task;
		
	}
	
	
	// prints the map
	public void printMap(MapNodes map) {
		int x = 0;
		int y = 0;
		while (y <= 3) {
			while (x <= 7) {
				// print map
				
				if (x == 7) {
					y+=1;
					x = 0;
				}
			}
		}
	}
	
	// asks user for game id
	public String getGameID() {
		print("Please enter game id");
		String id = sc.next();
		print("#########################");
		return id;
	}
	
		
}
