//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.31 at 10:53:24 AM CEST 
//


package messages.avatarMove.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}uniquePlayerID"/>
 *         &lt;element ref="{}move"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uniquePlayerID",
    "move"
})
@XmlRootElement(name = "avatarMove")
public class AvatarMove {

    @XmlElement(required = true)
    protected String uniquePlayerID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected MoveValues move;

    /**
     * Gets the value of the uniquePlayerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniquePlayerID() {
        return uniquePlayerID;
    }

    /**
     * Sets the value of the uniquePlayerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniquePlayerID(String value) {
        this.uniquePlayerID = value;
    }

    /**
     * Gets the value of the move property.
     * 
     * @return
     *     possible object is
     *     {@link MoveValues }
     *     
     */
    public MoveValues getMove() {
        return move;
    }

    /**
     * Sets the value of the move property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoveValues }
     *     
     */
    public void setMove(MoveValues value) {
        this.move = value;
    }

}
