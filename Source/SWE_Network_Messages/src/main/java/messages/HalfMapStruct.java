package messages;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement (name = "halfMap")
@XmlAccessorType(XmlAccessType.NONE)
public class HalfMapStruct {

	@XmlElement(name="uniquePlayerID")
	private final String uniquePlayerID;
	
	@XmlElement(name="newMapNodes")
	private final newMapNode[] newMapNodes;
	
	public HalfMapStruct() {
		this.uniquePlayerID = "";
		this.newMapNodes = new newMapNode[0];
	}

	public HalfMapStruct(String uniquePlayerID, newMapNode[] newMapNodes) {
		this.uniquePlayerID = uniquePlayerID;
		this.newMapNodes = newMapNodes;
	}

	public String getUniquePlayerID() {
		return uniquePlayerID;
	}

	public newMapNode[] getNewMapNodes() {
		return newMapNodes;
	}

	
	

}

