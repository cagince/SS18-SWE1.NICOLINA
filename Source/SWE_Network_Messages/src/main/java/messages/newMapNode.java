package messages;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement (name = "newMapNode")
@XmlAccessorType(XmlAccessType.NONE)
public class newMapNode {

	@XmlElement(name="X")
	private final int x;
	
	@XmlElement(name="Y")
	private final int y;
	
	@XmlElement(name="terrain")
	private final String terrain;
	
	@XmlElement(name="fortPresent")
	private final boolean fortPresent;

	public newMapNode() {
		this.x = -1;
		this.y = -1;
		this.terrain = "";
		this.fortPresent = false;
	}
	
	public newMapNode(int x, int y, String terrain, boolean fortPresent) {
		this.x = x;
		this.y = y;
		this.terrain = terrain;
		this.fortPresent = fortPresent;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public String getTerrain() {
		return terrain;
	}

	public boolean isFortPresent() {
		return fortPresent;
	}
	
	
	

}

