package messages;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement (name = "avatarMove")
@XmlAccessorType(XmlAccessType.NONE)
public class AvatarMove {

	@XmlElement(name="move")
	private final String move;
	@XmlElement(name="uniquePlayerID")
	private final String uniquePlayerID;
	
	public AvatarMove() {
		this.move = "";
		this.uniquePlayerID = "";
	}
	
	public AvatarMove(String move, String id) {
		this.move = move;
		this.uniquePlayerID = id;
	}

	public String getMove() {
		return move;
	}

	public String getUniquePlayerID() {
		return uniquePlayerID;
	}
	
	
	

}